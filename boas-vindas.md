## Manual de boas-vindas equipe de recargas

Seja bem vindo a equipe de recargas da M4U. Esse manual tem o objetivo de deixar seus primeiros dias mais simples. Entendemos que cada processo de boas vindas tem suas particularidades, então fique a vonte para nos ajudar nesse processo.

#### Mentoria
Um integrante da equipe será seu mentor durante as suas 2 primeiras semanas. O papel desta pessoa é lhe orientar e ser seu ponto focal para qualquer tipo de ajuda que você precise. Os GPs irão lhe informar quem é esta pessoa (vide anexo “estrutura da equipe” na última página).

### Dia 1
#### Parte da manhã
#### Preparação da apresentação
Faça alguns slides que mostre sua personalidade, quem você é e alguns fatos engraçados.
Regras? Não tem regras, mas use o bom senso, que tal no máximo uns 5 slides? Use sua criatividade!

#### Leia os guias
* [Discord](/guia-discord.md)
* [Jira](/guia-jira.md)
* [BitBucket](/guia-bitbucket.md)
* [VPN](/guia-vpn.md)

#### Parte da tarde
#### Reunião para você se apresentar ao time
Utilize a apresentação que você preparou de manhã. Lembre-se: a primeira impressão é a que fica!

#### Reunião sobre a empresa e o time
Uma visão geral com os GPs sobre a organização do time/empresa, forma de trabalho e sobre produtos/serviços da M4U.

#### Apresentação dos produtos
Chegou a hora de você conhecer um pouco sobre os produtos, afinal esse é o resultado de todo nosso trabalho.
Veja uma apresentação realizada para novos membros, esse é um formato que ainda estamos ajustando, mas isso já irá ajudar.

* [Parte 1](https://infram4u-my.sharepoint.com/personal/alexandre_goes_m4u_com_br/_layouts/15/onedrive.aspx?slrid=4e19879e-c0dd-6000-20b9-dea42e461778&FolderCTID=0x012000375F87F1FD47834F880F042D766E578A&id=%2Fpersonal%2Falexandre_goes_m4u_com_br%2FDocuments%2FProdutos%2FMultirecarga%2FDocumenta%C3%A7%C3%A3o%2FApresenta%C3%A7%C3%B5es%2Fparte%201%2Emp4&parent=%2Fpersonal%2Falexandre_goes_m4u_com_br%2FDocuments%2FProdutos%2FMultirecarga%2FDocumenta%C3%A7%C3%A3o%2FApresenta%C3%A7%C3%B5es)
* [Parte 2](https://infram4u-my.sharepoint.com/personal/alexandre_goes_m4u_com_br/_layouts/15/onedrive.aspx?slrid=4e19879e-c0dd-6000-20b9-dea42e461778&FolderCTID=0x012000375F87F1FD47834F880F042D766E578A&id=%2Fpersonal%2Falexandre_goes_m4u_com_br%2FDocuments%2FProdutos%2FMultirecarga%2FDocumenta%C3%A7%C3%A3o%2FApresenta%C3%A7%C3%B5es%2Fparte%202%2Emp4&parent=%2Fpersonal%2Falexandre_goes_m4u_com_br%2FDocuments%2FProdutos%2FMultirecarga%2FDocumenta%C3%A7%C3%A3o%2FApresenta%C3%A7%C3%B5es)
* [Slides](https://infram4u-my.sharepoint.com/personal/alexandre_goes_m4u_com_br/_layouts/15/Doc.aspx?sourcedoc=%7B9b98d719-08c4-4a2e-83f0-6d02fcae06fa%7D&action=default&uid=%7B9B98D719-08C4-4A2E-83F0-6D02FCAE06FA%7D&ListItemId=8054&ListId=%7BFC3D3F09-4D67-4923-9CC8-E81F6BFF9245%7D&odsp=1&env=prod&CT=1534904234233&OR=DocLib)

### Dia 2
#### Parte da manhã
#### Visão geral da arquitetura
Após uma visão geral dos produtos e serviços, vamos avançar para uma reunião de arquitetura, com um dos líderes técnicos da equipe (vide anexo “estrutura da equipe” na última página).

#### Parte da tarde
#### Código, como os serviços se cominicam
Para finalizar, uma reunião com uma pessoa que conhece bem a comunicação entre os sistemas


### Dia 3
Chegou a hora de colocar a mão na massa! Faça um pair programming com alguém do seu squad para ir se familiarizando com o código e os sistemas envolvidos. Assim que você se sentir minimamente seguro para seguir em frente sozinho, avise a equipe e assuma uma tarefa.

### Conclusão
Esperamos que você tenha a melhor experiência possível no início da sua nova jornada aqui na M4U. Nosso processo de boas-vindas está em constante aprimoramento, então fique a vontade para sugerir melhorias! Os próximos colaboradores agradecem! 
Vamos conversar com você mensalmente para coletar feedbacks sobre sua experiência aqui na M4U e, com isso, identificar falhas e apoiá-lo nos seus problemas.

### Nosso espirito

> "Truly great companies will welcome you with open arms, keep you engaged, consider your feelings, and make you too excited to sleep the night before the second day of work."

Tradução livre: Empresas muito boas irão lhe dar as boas vindas de braços abertos, manter você engajado, considerar suas expectativas, e fazer você ficar tão animado que será difícil dormir na noite anterior ao segundo dia de trabalho.

> "They kept me busy with a highly-structured schedule for the first 2 weeks, followed by regular check-in meetings for the following month until I was comfortable enough to set my own pace. I was never left wondering if I was doing the right thing or what to do next."

Tradução livre: Eles me mantiveram ocupados com uma agenda bem elaborada pelas primeiras duas semanas, seguidas de reuniões regulares pelo próximo mês até eu me sentir confortável o bastante para continuar sozinho. Eu nunca me perguntei se eu estava fazendo a coisa certa ou qual a próxima coisa a fazer.

Se falharmos nas metas acima, por favor nos avise para melhorarmos!

#### Links úteis
* [Documentação técnica](https://infram4u-my.sharepoint.com/:f:/g/personal/alexandre_goes_m4u_com_br/EoQwCeFdgYpHvSaADcCZuOMBtTSykaKkMroKifYy-YStBQ?e=APpfM1)
* [Estrutura da equipe](https://infram4u-my.sharepoint.com/:x:/g/personal/alexandre_goes_m4u_com_br/EQcerpP8RHdMn-cADkNSSc4BYHzDRoDbSboTQ1PmsGufdQ?e=YMwTk0)

Fluxo de telas:

* [Fluxo App Claro Recarga](https://projects.invisionapp.com/share/GVN39376FUH#/screens)
* [Fluxo Web Claro Recarga](https://projects.invisionapp.com/share/YTM7Z025HU2#/screens)
