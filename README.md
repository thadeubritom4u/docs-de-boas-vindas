## Boas Vindas

Se você é um novo membro, começe [aqui](/boas-vindas.md)

Se você está envolvido na operação para receber um novo membro, clique [aqui](/operacao-boas-vindas.md)
