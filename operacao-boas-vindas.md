## Operação Boas Vindas
Esse é o guia de operação para receber um novo membro na equipe de recargas. Os passos a seguir garantirão que o novo membro tenha uma aterrissagem mais tranquila no nosso mundo.

#### Liberação de acesso aos serviço
* [Email da M4U]()
* [Discord]()
* [Jira]()
* [Bitbucket]()
* [VPN]()

#### Encontrar um mentor
Selecionar um mentor para ser o ponto focal do novo membro auxiliando-o em seus primeiros passos.

**Obs:** Criar um rodízio para os próximos mentores melhora a integração da equipe, mas ao mesmo tempo é importante que o mentor esteja disposto a realizar a mentoria.

#### Agendar reuniões
Sabemos o quão precioso é o tempo e o quão difícil é conciliar os horários entre todos, mas a presença nas reuniões é fundamental para a integração da equipe. Seria excelente contar com todas as pessoas nas suas respectivas reuniões.

* [Apresentação ao time]() (1º Dia - Tarde)
* [Apresentação sobre a empresa e o time]() (1º Dia - Tarde)
* [Apresentação da arquitetura]() (2º Dia - Manhã)
* [Pair programing]() (3º Dia)
* [Scrum (Daily, Review, Retrospective e Planning)]()
